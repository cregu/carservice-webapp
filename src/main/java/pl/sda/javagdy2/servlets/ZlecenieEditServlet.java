package pl.sda.javagdy2.servlets;

import pl.sda.javagdy2.database.EntityDao;
import pl.sda.javagdy2.database.model.Zlecenie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/zlecenie/edit")
public class ZlecenieEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long zlecenieId = Long.parseLong(req.getParameter("zlecenieId"));

            EntityDao dao = new EntityDao();
            Zlecenie zlecenie = dao.getById(Zlecenie.class, zlecenieId);

            req.setAttribute("zlecenieToEdit", zlecenie);
            req.setAttribute("klientIdentifier", zlecenie.getKlient().getId());
            req.setAttribute("opisUsterki", zlecenie.getOpisUsterki());
            req.setAttribute("nrRejestracyjny", zlecenie.getNrRejestracyjny());
            req.setAttribute("czyOplacone", zlecenie.isCzyOplacone());


            req.getRequestDispatcher("/zlecenie_form.jsp").forward(req, resp);
        } catch (NumberFormatException | NullPointerException ne) {
            resp.sendRedirect("/klient/list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long editedZlecenieId = Long.parseLong(req.getParameter("editedZlecenie"));

            EntityDao dao = new EntityDao();
            Zlecenie zlecenie = dao.getById(Zlecenie.class, editedZlecenieId);

            String opisUsterki = req.getParameter("opisUsterki");
            zlecenie.setOpisUsterki(opisUsterki);
            boolean czyOplacone = Boolean.parseBoolean(req.getParameter("czyOplacone"));
            zlecenie.setCzyOplacone(czyOplacone);
            String nrRejestracyjny = req.getParameter("nrRejestracyjny");
            zlecenie.setNrRejestracyjny(nrRejestracyjny);

            dao.saveOrUpdate(zlecenie);

            resp.sendRedirect("/klient/details?identifier=" + zlecenie.getKlient().getId());
        } catch (NumberFormatException | NullPointerException ne) {
            resp.sendRedirect("/klient/list");
        }
    }
}
