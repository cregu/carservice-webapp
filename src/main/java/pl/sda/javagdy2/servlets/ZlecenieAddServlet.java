package pl.sda.javagdy2.servlets;

import pl.sda.javagdy2.database.EntityDao;
import pl.sda.javagdy2.database.model.Klient;
import pl.sda.javagdy2.database.model.Zlecenie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebServlet("/zlecenie/add")
public class ZlecenieAddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String klientId = req.getParameter("klientId");
        if (klientId == null || klientId.isEmpty()) {
            resp.sendRedirect("/klient/list");
            return;
        }
        req.setAttribute("klient_identifier", klientId);

        req.getRequestDispatcher("/zlecenie_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String klientId = req.getParameter("klientZlecenieId");  // dodany kod - test
        Long klientIdLong = Long.parseLong(klientId);               // dodany kod - test
        EntityDao dao = new EntityDao();
        Zlecenie zlecenie = Zlecenie.builder()
                .opisUsterki(req.getParameter("opisUsterki"))
                .nrRejestracyjny(req.getParameter("nrRejestracyjny"))
                .czyOplacone(req.getParameter("czyOplacone") !=null)
                .dataDodania(LocalDate.parse(req.getParameter("dataDodania")))
                .klient(dao.getById(Klient.class,klientIdLong))   // dodany kod - test
                .build();
        dao.saveOrUpdate(zlecenie);

        resp.sendRedirect("/klient/details?identToEdit=" + klientId);  // dodany kod - test
    }
}
