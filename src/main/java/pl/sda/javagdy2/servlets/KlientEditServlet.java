package pl.sda.javagdy2.servlets;

import pl.sda.javagdy2.database.EntityDao;
import pl.sda.javagdy2.database.model.Klient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/klient/edit")
public class KlientEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long klientId = Long.parseLong(req.getParameter("klientId"));


            EntityDao dao = new EntityDao();
            Klient klient = dao.getById(Klient.class, klientId);

            req.setAttribute("klientToEdit", klient);
            req.setAttribute("imie", klient.getImie());
            req.setAttribute("nazwisko", klient.getNazwisko());
            req.setAttribute("iloscSamochodow", klient.getIloscSamochodow());
            req.setAttribute("czyDajeNapiwki", klient.isCzyDajeNapiwki());
            req.setAttribute("ocenaKlienta", klient.getOcenaKlienta());

            req.getRequestDispatcher("/klient_form.jsp").forward(req, resp);
        } catch (NumberFormatException | NullPointerException ne) {
            resp.sendRedirect("/klient/list");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long editedKlientId = Long.parseLong("editedKlient");

            EntityDao dao = new EntityDao();
            Klient klient = dao.getById(Klient.class, editedKlientId);

            String imie = req.getParameter("imie");
            klient.setImie(imie);
            String nazwisko = req.getParameter("nazwisko");
            klient.setNazwisko(nazwisko);
            int iloscSamochodow = Integer.parseInt(req.getParameter("iloscSamochodow"));
            klient.setIloscSamochodow(iloscSamochodow);
            boolean czyDajeNapiwki = Boolean.parseBoolean(req.getParameter("czyDajeNapiwki"));
            klient.setCzyDajeNapiwki(czyDajeNapiwki);
            int ocenaKlienta = Integer.parseInt(req.getParameter("ocenaKlienta"));
            klient.setOcenaKlienta(ocenaKlienta);

            dao.saveOrUpdate(klient);

            resp.sendRedirect("/klient/details?identToEdit=" + klient.getId());
        } catch (NumberFormatException | NullPointerException ne) {
            resp.sendRedirect("/klient/list");
        }
    }
}
