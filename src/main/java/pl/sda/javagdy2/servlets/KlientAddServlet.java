package pl.sda.javagdy2.servlets;

import pl.sda.javagdy2.database.EntityDao;
import pl.sda.javagdy2.database.model.Klient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/klient/add")
public class KlientAddServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String klientId = req.getParameter("klientId");
        if (klientId == null || klientId.isEmpty()) {
            req.getRequestDispatcher("/klient_form.jsp").forward(req, resp);
            return;
        }
        req.setAttribute("klientAddId", klientId);
        req.getRequestDispatcher("/klient_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String klientToEditId = req.getParameter("klientToEditId");
//        Long klientToEditIdLong = Long.parseLong(klientToEditId);
        EntityDao dao = new EntityDao();
        Klient klient = Klient.builder()
                .imie(req.getParameter("imie"))
                .nazwisko(req.getParameter("nazwisko"))
                .iloscSamochodow(Integer.parseInt(req.getParameter("iloscSamochodow")))
                .czyDajeNapiwki(req.getParameter("czyDajeNapiwki") != null)
                .ocenaKlienta(Integer.parseInt(req.getParameter("ocenaKlienta")))
                .build();
        dao.saveOrUpdate(klient);

        resp.sendRedirect("/klient/list");
    }
}
