package pl.sda.javagdy2.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Zlecenie implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String opisUsterki;
    private String nrRejestracyjny;
    private boolean czyOplacone;

    @CreationTimestamp
    private LocalDate dataDodania;

    @ManyToOne
    private Klient klient;      // powiazany z dodanym w builderze zlecenia kodem, tworzenie relacji


}
