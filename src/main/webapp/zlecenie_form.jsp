<%--
  Created by IntelliJ IDEA.
  User: Koper
  Date: 07.05.2020
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Formularz zlecenia</title>
</head>
<body>
<h1>Formularz zlecenia dla klienta o id: <c:out value="${requestScope.klient_identifier}"/></h1>
<jsp:include page="/menu.jsp"/>

<br/>
<form action="${ requestScope.zlecenieToEdit == null ? '/zlecenie/add' : '/zlecenie/edit'}" method="post">
    <input type="hidden" name="klientZlecenieId" value="<c:out value="${requestScope.klient_identifier}"/>">
    <input type="hidden" name="editedZlecenie" value="<c:out value="${requestScope.zlecenieToEdit.id}"/>">
<%--<form action="/zlecenie/add" method="post">--%>
    Opis usterki:                   <input type="text" name="opisUsterki" value="${requestScope.zlecenieToEdit.opisUsterki}">
    <br/>
    Nr Rejestracyjny:               <input type="text" name="nrRejestracyjny" value="${requestScope.zlecenieToEdit.nrRejestracyjny}">
    <br/>
    Czy oplacone:                   <input type="checkbox" name="czyOplacone" ${(requestScope.zlecenieToEdit.czyOplacone)? "checked":""} value="true">
    <br/>
    Data dodania (RRRR-MM-DD):      <input type="text" name="dataDodania">
    <br/>
    <input type="submit">
</form>

</body>
</html>
