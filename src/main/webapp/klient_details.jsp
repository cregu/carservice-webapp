<%--
  Created by IntelliJ IDEA.
  User: Koper
  Date: 07.05.2020
  Time: 14:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Klient details</title>
</head>
<body>
<h1>Szczegolowe informacje o kliencie o id: <c:out value="${requestScope.klientDetails.id}"/></h1>
<jsp:include page="/menu.jsp"/>

<br/>
<table>
    <tr>
        <td>Imie:</td>
        <td><c:out value="${requestScope.klientDetails.imie}"/></td>
    </tr>
    <tr>
        <td>Nazwisko</td>
        <td><c:out value="${requestScope.klientDetails.nazwisko}"/></td>
    </tr>
    <tr>
        <td>Ilosc Samochodow</td>
        <td><c:out value="${requestScope.klientDetails.iloscSamochodow}"/></td>
    </tr>
    <tr>
        <td>Czy daje napiwki</td>
        <td><c:out value="${requestScope.klientDetails.czyDajeNapiwki}"/></td>
    </tr>
    <tr>
        <td>Ocena klienta</td>
        <td><c:out value="${requestScope.klientDetails.ocenaKlienta}"/></td>
    </tr>
</table>
<br/>

<a href="/zlecenie/add?klientId=<c:out value="${requestScope.klientDetails.id}"/>">Dodaj zlecenie temu klientowi</a>
<br/>
<table style="border: 1px solid">
    <thead>
    <tr>
        <td>Id</td>
        <td>Opis usterki</td>
        <td>Nr rejestracyjny</td>
        <td>Czy oplacone</td>
        <td>Data dodania</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="zlecenie" items="${requestScope.klientDetails.zlecenieList}">
        <tr>
            <td><c:out value="${zlecenie.id}"/></td>
            <td><c:out value="${zlecenie.opisUsterki}"/></td>
            <td><c:out value="${zlecenie.nrRejestracyjny}"/></td>
            <td><c:out value="${zlecenie.czyOplacone}"/></td>
            <td><c:out value="${zlecenie.dataDodania}"/></td>
            <td><a href="/zlecenie/remove?zlecenieId=<c:out value="${zlecenie.id}"/>">Usun zlecenie</a></td>

            <td><a href="/zlecenie/edit?zlecenieId=<c:out value="${zlecenie.id}"/>">Edytuj zlecenie</a></td>

        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
