<%--
  Created by IntelliJ IDEA.
  User: Koper
  Date: 06.05.2020
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Lista Klientow</title>
</head>
<body>
<h1>Lista Klientow</h1>
<jsp:include page="menu.jsp"/>
<table>
    <thead>
    <tr>
        <td>Id</td>
        <td>Imie</td>
        <td>Nazwisko</td>
        <td>Ilosc Samochodow</td>
        <td>Czy daje napiwki</td>
        <td>Ocena klienta</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="klient" items="${requestScope.klienci}">
        <tr>
            <td class="border"><c:out value="${klient.id}"/></td>
            <td><c:out value="${klient.imie}"/></td>
            <td><c:out value="${klient.nazwisko}"/></td>
            <td><c:out value="${klient.iloscSamochodow}"/></td>
            <td><c:out value="${klient.czyDajeNapiwki}"/></td>
            <td><c:out value="${klient.ocenaKlienta}"/></td>
            <td><a href="/klient/remove?identToRemove=<c:out value="${klient.id}"/>">Usun</a> </td>
            <td><a href="/klient/details?identifier=<c:out value="${klient.id}"/>">Szczegoly</a> </td>
            <td><a href="/klient/edit?identifier=<c:out value="${klient.id}"/>">Edytuj</a></td>
        </tr>
    </c:forEach>
    </tbody>

</table>

</body>
</html>
