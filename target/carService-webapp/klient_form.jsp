<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<%--
  Created by IntelliJ IDEA.
  User: Koper
  Date: 06.05.2020
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz dodawania klienta</title>
</head>
<body>
<h1>Klient form</h1>
<jsp:include page="/menu.jsp"/>
<br/>
<form action="${requestScope.klientToEdit == null ? '/klient/add' : '/klient/edit'}" method="post">
    <input type="hidden" name="klientId" value="<c:out value="${requestScope.klientAddId}"/>">
    <input type="hidden" name="editedKlient" value="<c:out value="${requestScope.klientToEdit.id}"/>">

    Imie: <input type="text" name="imie" value="${requestScope.klientToEdit.imie}">
    <br/>
    Nazwisko: <input type="text" name="nazwisko" value="${requestScope.klientToEdit.nazwisko}">
    <br/>
    Ilosc samochodow: <input type="number" name="iloscSamochodow" value="${requestScope.klientToEdit.iloscSamochodow}">
    <br/>
    Czy daje napiwki: <input type="checkbox" name="czyDajeNapiwki" value="${requestScope.klientToEdit.czyDajeNapiwki}">
    <br/>
    Ocena klienta: <input type="number" min="1" max="10" name="ocenaKlienta" value="${requestScope.klientToEdit.ocenaKlienta}">
    <br/>
    <input type="submit">
</form>

</body>
</html>
